# community-牛客社区项目

#### 介绍
牛客网项目

牛客社区项目，跟着思路纯手打，主要是项目涉及到了Redis 缓存机制，实际应用和Kafka 消息队列，Elasticsearch，Spring Security等实际应用，还有一些企业内设计业务的思路，是个不错的练手项目

#### 软件架构
软件架构说明  
Spring boot 2.6.1 + Thymeleaf  
Redis 5.0.10 + Kafka 2.3.0 + Elasticsearch 7.15.2

#### 安装教程

1.  拉取项目
2.  启动redis，kafka，elasticsearch
3.  over

 **无聊的说明** 

没有兴趣写博客，因此关于es版本一些坑直接在这里提一下  
后端完全根据牛客视频内容纯个人手打，Thymeleaf是从一开始的资料不断改的，跟着看完就改，测测补补也差不多了。  
独自完成某些课时下没说的作业的功能，当练手...还有自己也稍微修正了下视频没提到的繁琐的前端重复工作小问题补全  
因为实在懒得降Spring版本，Elasticsearch版本为7.15.2，尝试去解决一大堆过期方法啥的不兼容的问题。  
对于es7网上的资料少得可怜，大多都是es6的，搜方法才有一些解释，要进一步探索还是得看文档  
参考文章：[https://blog.csdn.net/wpw2000/article/details/115704320](https://blog.csdn.net/wpw2000/article/details/115704320)   
参考文档：[https://docs.spring.io/spring-data/elasticsearch/docs/current/api/](https://docs.spring.io/spring-data/elasticsearch/docs/current/api/)    
有些方法直接拿过来改了改，毕竟还是有些不同..  
比如Document的某些属性过期了还是翻文档才发现不知什么版本开始要配合Setting注解使用了   
还有需要根据和视频不同修改出自己返回的逻辑，详细看代码吧..

quartz版本深坑 :  
刚开始引入的时候突然报There is no DataSource named 'null'的错误  
然后把注释去掉就又能正常执行，一度认为自己哪里写错了，对着视频看了挺久还是没法解决  
然后就想到了配置的问题，配置了数据源还是报错找了好久才想到会不会是版本问题，把代码贴一份到以前2.5以下的工程里面又能正常执行orz  
但无奈这么后面了不想强降版本，es都熬过来了，还是选择找方法解决   
终于还是找到了解决方案，新版本真的没啥人权..错了只能强行自己找  
2.6.0 spring以上需把配置数据源实现的class从
org.quartz.impl.jdbcjobstore.JobStoreTX -> 
org.springframework.scheduling.quartz.LocalDataSourceJobStore  完事  

没做图片分享的功能，没页面就测试用的就不浪费时间了  
感觉没啥必要特意去安一个平时完全不用的软件..基本是外部依靠cmd执行所以没做，自己大概能看懂就行  
7.23章 图片上传没按视频写，按原来逻辑用同步表单上传至阿里云oss  
主要是不怎么想强行异步上传而大改方法..  
上传个图片大小限制又不怎么耗时，加上这完全是用户操作，完全可在接受范围内、、  

21.12.20-正式完工，所有功能完成，测试正常

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


