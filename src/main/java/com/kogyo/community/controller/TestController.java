package com.kogyo.community.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@Controller
@RequestMapping("/test")
public class TestController {
    @RequestMapping("/http")
    public void http(HttpServletRequest request, HttpServletResponse response){
        System.out.println(request.getMethod());
        System.out.println(request.getServletPath());
        Enumeration<String> enumeration = request.getHeaderNames();
        while (enumeration.hasMoreElements()){
            String name = enumeration.nextElement();
            String value = request.getHeader(name);
            System.out.println(name + " : " + value);
        }

        System.out.println(request.getParameter("code"));

        response.setContentType("text/html;charset=utf-8");
        try {
            PrintWriter writer = response.getWriter();
            writer.write("<h1>test my！你好</h1>");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RequestMapping(path = "/student",method = RequestMethod.GET)
    @ResponseBody
    public String getList(
            @RequestParam(name = "current",required = false,defaultValue = "1") int current,
            @RequestParam(name = "limit",required = false,defaultValue = "10") int limit
    ){
        return current+" "+limit;
    }
    @RequestMapping(path = "/student/{id}",method = RequestMethod.GET)
    @ResponseBody
    public String getStudent(@PathVariable("id")int id){
        System.out.println(id);
        return "success";
    }
    @RequestMapping(path = "/student",method = RequestMethod.POST)
    @ResponseBody
    public String saveStudent(String name,int age){
        System.out.println(name+ " "+age);
        return "success";
    }
    @RequestMapping(path = "/teacher/{id}",method = RequestMethod.GET)
    public ModelAndView getTeacher(@PathVariable("id")int id){
        ModelAndView view = new ModelAndView();
        view.addObject("name","祸鱼");
        view.addObject("age","24");
        view.setViewName("demo/view");
        return view;
    }
    @RequestMapping(path = "/school",method = RequestMethod.GET)
    public String getTeacher(Model model){

        model.addAttribute("name","祸鱼");
        model.addAttribute("age","24");
        return "demo/view";
    }
}
