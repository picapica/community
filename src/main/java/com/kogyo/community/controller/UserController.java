package com.kogyo.community.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.GeneratePresignedUrlRequest;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import com.kogyo.community.annnotation.LoginRequired;
import com.kogyo.community.entity.Comment;
import com.kogyo.community.entity.DiscussPost;
import com.kogyo.community.entity.Page;
import com.kogyo.community.entity.User;
import com.kogyo.community.service.*;
import com.kogyo.community.util.CommunityConstant;
import com.kogyo.community.util.CommunityUtil;
import com.kogyo.community.util.HostHolder;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.*;

@Controller
@RequestMapping("/user")
public class UserController implements CommunityConstant {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);
    @Value("${community.path.upload}")
    private String uploadPath;
    @Value("${community.path.domain}")
    private String domain;
    @Value("${server.servlet.context-path}")
    private String contextPath;
    @Autowired
    private UserService userService;
    @Autowired
    private HostHolder hostHolder;
    @Autowired
    private LikeService likeService;
    @Autowired
    private FollowService followService;

    @Autowired
    private DiscussPostService discussPostService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private OSS ossClient;

    @Value ("${aliyun.bucket-name}")
    private String bucketName;

    @Value("${aliyun.endpoint}")
    private String endpoint;
    @LoginRequired
    @RequestMapping(path = "/setting",method = RequestMethod.GET)
    public String getSettingPage(){
        return "site/setting";
    }

    @LoginRequired
    @RequestMapping(path = "/setting",method = RequestMethod.POST)
    public String settingPassword(String oldPassword,String newPassword
            ,Model model,@CookieValue("ticket") String ticket){
        User user = hostHolder.getUser();
        if(user==null){
            return "/site/login";
        }
        if(user.getPassword().equals(CommunityUtil.md5(oldPassword+user.getSalt()))){
            userService.changePassword(user.getEmail(),newPassword);
            userService.logout(ticket);
            model.addAttribute("msg","修改密码成功，请尝试登陆");
            model.addAttribute("target","/login");
            return "/site/operate-result";
        } else {
            model.addAttribute("passwordMsg","旧密码输入不正确");
            return "/site/setting";
        }
    }

    @LoginRequired
    @RequestMapping(path = "/upload",method = RequestMethod.POST)
    public String uploadHeader(MultipartFile headerImage, Model model){
        if(headerImage==null){
            model.addAttribute("error","你还没有选择图片");
            return "/site/setting";
        }

        String filename = headerImage.getOriginalFilename();
        if(filename==null){
            model.addAttribute("error","图片名错误");
            return "/site/setting";
        }
        String suffix = filename.substring(filename.lastIndexOf("."));
        if(StringUtils.isBlank(suffix)){
            model.addAttribute("error","文件格式不正确");
            return "/site/setting";
        }
        //生成随机文件名
        filename = CommunityUtil.generateUUID() + suffix;
//        File dest = new File(uploadPath+"/"+filename);
        filename = "header/" + filename;
        try {
//            headerImage.transferTo(dest);
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, filename,headerImage.getInputStream());
            Map<String, String> map = new HashMap<>();
            //设置为公开读可见
            map.put("x-oss-object-acl","public-read");
            putObjectRequest.setHeaders(map);
            PutObjectResult putResult = ossClient.putObject(putObjectRequest);
        } catch (IOException e) {
           logger.error("上传文件失败："+e.getMessage());
           throw new RuntimeException("上传文件失败，服务器发生异常！",e);
        } finally {
            ossClient.shutdown();
        }
        //更新当前头像路径
        User user = hostHolder.getUser();
//        String headerUrl = getUrl(filename);
        String headerUrl = "https://" + bucketName + "." + endpoint + "/" + filename;
//        String headerUrl = domain + contextPath + "/user/header/" + filename;
        int i = userService.updateHeader(user.getId(), headerUrl);
        System.out.println(i);
        return "redirect:/index";
    }

//    @LoginRequired
//    @RequestMapping(path = "/upload",method = RequestMethod.POST)
//    public String uploadHeader(MultipartFile headerImage, Model model){
//        if(headerImage==null){
//            model.addAttribute("error","你还没有选择图片");
//            return "/site/setting";
//        }
//
//        String filename = headerImage.getOriginalFilename();
//        if(filename==null){
//            model.addAttribute("error","图片名错误");
//            return "/site/setting";
//        }
//        String suffix = filename.substring(filename.lastIndexOf("."));
//        if(StringUtils.isBlank(suffix)){
//            model.addAttribute("error","文件格式不正确");
//            return "/site/setting";
//        }
//        //生成随机文件名
//        filename = CommunityUtil.generateUUID() + suffix;
//        File dest = new File(uploadPath+"/"+filename);
//        try {
//            headerImage.transferTo(dest);
//        } catch (IOException e) {
//            logger.error("上传文件失败："+e.getMessage());
//            throw new RuntimeException("上传文件失败，服务器发生异常！",e);
//        }
//        //更新当前头像路径
//        User user = hostHolder.getUser();
//        String headerUrl = domain + contextPath + "/user/header/" + filename;
//        int i = userService.updateHeader(user.getId(), headerUrl);
//        System.out.println(i);
//        return "redirect:/index";
//    }

    @RequestMapping(path = "/header/{filename}",method = RequestMethod.GET)
    public void getHeader(@PathVariable("filename") String filename, HttpServletResponse response){
        filename = uploadPath + "/" + filename;
        String suffix = filename.substring(filename.lastIndexOf("."));
        response.setContentType("image/"+suffix);
        try(FileInputStream fileInputStream = new FileInputStream(filename);
        OutputStream outputStream = response.getOutputStream();
        ) {
            byte[] buffer = new byte[1024];
            int b = 0;
            while ((b=fileInputStream.read(buffer))!=-1){
                outputStream.write(buffer,0,b);
            }
        } catch (IOException e) {
           logger.error("读取头像失败：" + e.getMessage());
        }
    }
    //个人主页
    @RequestMapping(path = "/profile/{userId}",method = RequestMethod.GET)
    public String getProfilePage(@PathVariable("userId")int userId,Model model){
        User user = userService.findUserById(userId);
        if(user==null){
            throw new RuntimeException("用户不存在");
        }
        //用户
        model.addAttribute("user",user);
        //点赞数目
        int likeCount = likeService.findUserLikeCount(userId);
        model.addAttribute("likeCount",likeCount);
        //查询关注数量
        long followeeCount = followService.findFolloweeCount(userId, ENTITY_TYPE_USER);
        model.addAttribute("followeeCount",followeeCount);
        //粉丝数量
        long followerCount = followService.findFollowerCount(ENTITY_TYPE_USER, userId);
        model.addAttribute("followerCount",followerCount);
        //是否关注
        boolean hasFollowed = false;
        if(hostHolder.getUser()!=null){
            hasFollowed = followService.hasFollowed(hostHolder.getUser().getId(), ENTITY_TYPE_USER, userId);
        }
        model.addAttribute("hasFollowed",hasFollowed);
        return "/site/profile";
    }

    // 我的帖子
    @RequestMapping(path = "/mypost/{userId}", method = RequestMethod.GET)
    public String getMyPost(@PathVariable("userId") int userId, Page page, Model model) {
        User user = userService.findUserById(userId);
        if (user == null) {
            throw new RuntimeException("该用户不存在！");
        }
        model.addAttribute("user", user);

        // 分页信息
        page.setPath("/user/mypost/" + userId);
        page.setRows(discussPostService.findDiscussPostRows(userId));

        // 帖子列表
        List<DiscussPost> discussList = discussPostService
                .findDiscussPosts(userId, page.getOffset(), page.getLimit(),0);
        List<Map<String, Object>> discussVOList = new ArrayList<>();
        if (discussList != null) {
            for (DiscussPost post : discussList) {
                Map<String, Object> map = new HashMap<>();
                map.put("discussPost", post);
                map.put("likeCount", likeService.findEntityLikeCount(ENTITY_TYPE_POST, post.getId()));
                discussVOList.add(map);
            }
        }
        model.addAttribute("discussPosts", discussVOList);

        return "/site/my-post";
    }

    // 我的回复
    @RequestMapping(path = "/myreply/{userId}", method = RequestMethod.GET)
    public String getMyReply(@PathVariable("userId") int userId, Page page, Model model) {
        User user = userService.findUserById(userId);
        if (user == null) {
            throw new RuntimeException("该用户不存在！");
        }
        model.addAttribute("user", user);

        // 分页信息
        page.setPath("/user/myreply/" + userId);
        page.setRows(commentService.findUserCount(userId));

        // 回复列表
        List<Comment> commentList = commentService.findUserComments(userId, page.getOffset(), page.getLimit());
        List<Map<String, Object>> commentVOList = new ArrayList<>();
        if (commentList != null) {
            for (Comment comment : commentList) {
                Map<String, Object> map = new HashMap<>();
                map.put("comment", comment);
                DiscussPost post = discussPostService.findDiscussPostById(comment.getEntityId());
                map.put("discussPost", post);
                commentVOList.add(map);
            }
        }
        model.addAttribute("comments", commentVOList);

        return "/site/my-reply";
    }

//    /**
//     * 获取oss中头像的url
//     * @param fileName
//     * @return
//     */
//    private String getUrl(String fileName) {
//        // 设置URL过期时间为2年  3600l* 1000*24*365*2
//        Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24 * 365 * 2);
//        // 生成URL
//        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, fileName);
//        generatePresignedUrlRequest.setExpiration(expiration);
//        URL url = ossClient.generatePresignedUrl(generatePresignedUrlRequest);
//        if (url != null)
//        {
//            return url.toString();
//        }
//        return null;
//    }
}
