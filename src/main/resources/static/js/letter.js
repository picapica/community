$(function(){
	$("#sendBtn").click(send_letter);
	$(".close").click(delete_msg);
});

function send_letter() {
	$("#sendModal").modal("hide");

	var toName = $("#recipient-name").val();
	var content = $("#message-text").val();
	$.post(
		CONTEXT_PATH + '/letter/send',
		{"toName":toName,"content":content},
		function (data){
			data = $.parseJSON(data);
			if(data.code == 0){
				$('#hintBody').text("发送成功！");
			}else{
				$('#hintBody').text("发送失败:"+data.msg);
			}
			$("#hintModal").modal("show");
			setTimeout(function(){
				$("#hintModal").modal("hide");
				location.reload();
			}, 2000);
		}
	);
}

function delete_msg() {
	// TODO 删除数据
	var id = $(this).children("#delete_id").val();
	console.log(id);
	$.post(
		CONTEXT_PATH + '/letter/del',
		{"id":id},
		function (data){
			data = $.parseJSON(data);
			if(data.code == 0){
				console.log(data.code);
			} else {
				console.log(data.msg);
			}
		}
	);
	$(this).parents(".media").remove();
}