package com.kogyo.community;

import com.kogyo.community.dao.DiscussPostMapper;
import com.kogyo.community.dao.LoginTicketMapper;
import com.kogyo.community.dao.MessageMapper;
import com.kogyo.community.dao.UserMapper;
import com.kogyo.community.entity.DiscussPost;
import com.kogyo.community.entity.LoginTicket;
import com.kogyo.community.entity.Message;
import com.kogyo.community.entity.User;
//import org.junit.Test;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.Date;
import java.util.List;

@SpringBootTest
@ContextConfiguration(classes = CommunityApplication.class)
public class MapperTest {
    @Autowired
    private UserMapper mapper;
    @Autowired
    private LoginTicketMapper loginTicketMapper;
    @Autowired
    private MessageMapper messageMapper;
    @Test
    public void testSelectUser(){
        User user = mapper.selectById(101);
        System.out.println(user);
        user = mapper.selectByName("liubei");
        System.out.println(user);
        user = mapper.selectByEmail("nowcoder101@sina.com");
        System.out.println(user);
    }
    @Test
    public void testInsertUser(){
        User user = new User();
        user.setUsername("test");
        user.setPassword("132456");
        user.setSalt("abc");
        user.setEmail("test@qq.com");
        user.setHeaderUrl("http://www.nowcoder.com/101.png");
        user.setCreateTime(new Date());
        int rows = mapper.insertUser(user);
        System.out.println(rows);
        System.out.println(user.getId());
    }
    @Test
    void updateUser(){
        int rows = mapper.updateStatus(150,1);
        System.out.println(rows);
        rows = mapper.updateHeader(150,"http://www.nowcoder.com/102.png");
        System.out.println(rows);
        rows = mapper.updatePassword(150,"123456");
        System.out.println(rows);
    }
    @Autowired
    private DiscussPostMapper discussPostMapper;
    @Test
    void testSelectPosts(){
        List<DiscussPost> discussPosts = discussPostMapper.selectDiscussPosts(149, 0, 10,0);
        for(DiscussPost post:discussPosts){
            System.out.println(post);
        }
        int rows = discussPostMapper.selectDiscussPostRows(149);
        System.out.println(rows);
    }

    @Test
    public void testInsertTicket(){
        LoginTicket loginTicket = new LoginTicket();
        loginTicket.setUserId(101);
        loginTicket.setTicket("abc");
        loginTicket.setStatus(0);
        loginTicket.setExpired(new Date(System.currentTimeMillis() + 1000 * 60 * 10));
        loginTicketMapper.insertLoginTicket(loginTicket);
    }
    @Test
    public void testSelect(){
        LoginTicket loginTicket = loginTicketMapper.selectByTicket("abc");
        System.out.println(loginTicket);
        loginTicketMapper.updateStatus("abc",1);
        loginTicket = loginTicketMapper.selectByTicket("abc");
        System.out.println(loginTicket);
    }

    @Test
    public void testMessage(){
        List<Message> list = messageMapper.selectConversations(111, 0, 20);
        for (Message message : list) {
            System.out.println(message);
        }

        int count = messageMapper.selectConversationCount(111);
        System.out.println(count);

        List<Message> letters = messageMapper.selectLetters("111_112", 0, 10);
        for (Message message : letters) {
            System.out.println(message);
        }
        count = messageMapper.selectLetterCount("111_112");
        System.out.println(count);

        count = messageMapper.selectLetterUnreadCount(131,"111_131");
        System.out.println(count);
    }
}
