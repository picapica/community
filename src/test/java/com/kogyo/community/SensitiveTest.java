package com.kogyo.community;

import com.kogyo.community.util.SensitiveFilter;
//import org.junit.Test;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@ContextConfiguration(classes = CommunityApplication.class)
public class SensitiveTest {

    @Autowired
    private SensitiveFilter sensitiveFilter;
    @Test
    public void testSensitiveFilter(){
        String text = "这里赌博，可以嫖娼，可以开票，可以干小穴，哈哈哈哈！";
        String filter = sensitiveFilter.filter(text);
        System.out.println(filter);

        text = "这里☆☆赌☆☆博☆☆，可以☆嫖☆☆娼☆，可以☆开☆票☆☆，可以干☆☆小☆穴☆，哈哈哈哈！我内赌博射";
        filter = sensitiveFilter.filter(text);
        System.out.println(filter);
    }
}
